package org.compain.service;


import org.compain.consumer.ReservationProxy;
import org.compain.model.ReservationNotification;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class ReservationService {

    private final ReservationProxy reservationProxy;

    private String token ="eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJiaWJsaW9AZ21haWwuY29tIiwiYWNjb3VudF9pZCI6Mywicm9sZSI6IkVtcGxveWVlIiwiaWF0IjoxNjI5MjIyMzQ2fQ.i4OtAv3GwhWhSt1IpQXZyaTWpFMH6iuJ2Li9uC8mKOGGzb4QG9XlH0yu2LOytmi_7zssI6D0iBxEKaN6XnsmZw";

    public ReservationService(ReservationProxy reservationProxy) {
        this.reservationProxy = reservationProxy;
    }
    @Scheduled(fixedDelay = 60000)
    //@Scheduled(cron = "0 0 9 * * ?")
    public void sendMailForNotifyReservationAvailable(){
        LocalDateTime today = LocalDateTime.now();
        List<ReservationNotification> reservationNotificationList = reservationProxy.getReservationToNotify(today, token);
        for(ReservationNotification reservationNotification : reservationNotificationList){
            reservationProxy.sendMailToNotifyUserReservationIsAvailable(reservationNotification, token);
        }
    }
}
