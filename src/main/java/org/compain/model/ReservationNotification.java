package org.compain.model;

import lombok.Data;

@Data
public class ReservationNotification {
    private Long idReservation;
    private String name;
    private String firstname;
    private String email;
    private String book;
}
