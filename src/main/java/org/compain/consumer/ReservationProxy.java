package org.compain.consumer;

import org.compain.model.ReservationNotification;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDateTime;
import java.util.List;

@Component
public class ReservationProxy {

    private String urlReservation = "http://localhost:8081/api/reservations/";

    private final RestTemplate restTemplate;

    public ReservationProxy(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public List<ReservationNotification> getReservationToNotify(LocalDateTime today, String token){
        HttpHeaders header = new HttpHeaders();
        header.setContentType(MediaType.APPLICATION_JSON);
        header.set(HttpHeaders.AUTHORIZATION,"Bearer " + token);
        HttpEntity<String> request = new HttpEntity<>(header);
        ResponseEntity<List<ReservationNotification>> response = restTemplate.exchange(urlReservation + "to-notify", HttpMethod.GET, request, new ParameterizedTypeReference<List<ReservationNotification>>() {});

        return response.getBody();
    }

    public void sendMailToNotifyUserReservationIsAvailable(ReservationNotification reservationNotification, String token){
        HttpHeaders header = new HttpHeaders();
        header.setContentType(MediaType.APPLICATION_JSON);
        header.set(HttpHeaders.AUTHORIZATION,"Bearer " + token);
        HttpEntity<ReservationNotification> request = new HttpEntity<>(reservationNotification, header);
        ResponseEntity<Void> response = restTemplate.exchange(urlReservation + "notify-user", HttpMethod.POST,request,Void.class);
    }
}

